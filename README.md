Verlinkste - Browser extension
==============================

## Development

### Preparation
```bash
$ chmod 744 scripts/watch.js
```

### Load Extension in Firefox

Open `about:debugging`, click `This Firefox` then click `Load Temporary Add-on...` and open the `build/manifest.json` file.

### Development

Open the browser console for your extension: go to `about:debugging` -> `This Firefox` -> select `Inspect` in your extension card.

To prevent the extension from closing on clickaway: open the menu (the three points) in the browser console and then click on `Disable Popup Auto-Hide`.
