const colors = require('tailwindcss/colors');

module.exports = {
  purge: ['./src/**/*.{js,jsx,ts,tsx}', './public/index.html'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        primary: colors.indigo['600'],
        'primary-darker': colors.indigo['700'],
        primaryContrastText: colors.white,

        secondary: colors.fuchsia['600'],
        secondaryContrastText: colors.white,

        warning: colors.red['500'],
        warningContrastText: colors.white,

        'primary-txt': colors.coolGray['900'],
        'link-txt': colors.blue['500'],
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
