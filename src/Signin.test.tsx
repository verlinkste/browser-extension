import React from 'react';
import { render, screen } from '@testing-library/react';
import App from './Signin';

test('smoke test', () => {
   render(<App />);

  const header = screen.getByText('Sign in to your account');
  expect(header).toBeInTheDocument();
});
