import React from 'react';
import { useHistory } from 'react-router-dom';

import { useContext } from '../context';
import { defaultUser, User } from '../context/user';
import axios from '../helpers/axios';

const Footer = () => {
  const history = useHistory();

  const { user, saveUser } = useContext();

  const handleLogout = () => {
    axios.post('/auth/logout', {})
      .then(() => {
        const user: User = {
          ...defaultUser,
          loggedIn: false,
        };
        saveUser(user);
        history.push('/');
      })
      .catch((error) => {
        // TODO error logging
        console.log(error)
      })
  }

  return (
    <footer className="border-t my-2">
      <div className="flex text-center font-semibold mt-1 justify-between">
        <a
          className="flex-grow text-link-txt cursor-pointer"
          target="_blank"
          rel="noopener noreferrer"
          href="https://verlinkste.com/imprint"
        >
          Imprint
        </a>
        <a
          className="flex-grow text-link-txt cursor-pointer"
          target="_blank"
          rel="noopener noreferrer"
          href="https://verlinkste.com/privacy-policy"
        >
          Privacy Policy
        </a>
        {user.loggedIn && (
          <>
            <button
              type="button"
              className="flex-grow text-link-txt cursor-pointer font-semibold outline-none focus:outline-none"
              onClick={handleLogout}
            >Logout
            </button>
          </>
        )}
      </div>
    </footer>
  );
};

export default Footer;
