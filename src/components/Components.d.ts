export interface Link {
  id: string
  title: string
  description: string
  href: string
  userRef: string
}

export interface Category {
  id: string
  title: string
  titleSlug: string
  visibility: 'private' | 'public'
  links: Array<Link>
}
