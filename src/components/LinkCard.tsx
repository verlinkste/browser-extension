import React from 'react';
import { useFormik } from 'formik';
import * as Yup from 'yup';

import { getGqlErrors, getHelperText, hasError } from '../helpers/formik';
import { useMutation, useQuery } from 'urql';
import { useContext } from '../context';
import { defaultEditLink } from '../context/link';

const CreateLink = `
  mutation ($title: String!, $description: String!, $href: String!, $categories: [String!]!) {
    createLink(title: $title, description: $description, href: $href, categories: $categories) {
      id
      title
      description
      href
    }
  }
`;

const UpdateLink = `
  mutation ($id: String!, $title: String!, $description: String!, $href: String!, $categories: [String!]!) {
    updateLink(id: $id, title: $title, description: $description, href: $href, categories: $categories) {
      id
      title
      description
      href
    }
  }
`;

const DeleteLink = `
  mutation ($id: String!) {
    deleteLink(id: $id) {
      id
    }
  }
`;

const GetCategoriesQuery = `
  query GetCategories($username: String!) {
    categories(username: $username) {
      id  
      title
    }
  }
`;

interface Values {
  id?: string
  title: string
  description: string
  href: string
  categories: Array<string>
}

interface Props {
  onCancel(): void
  onSubmit(): void
}

interface Category {
  id: string
  title: string
}

interface Result {
  categories: Array<Category>
}

const LinkCard = (props: Props) => {
  const {
   onSubmit, onCancel,
  } = props;

  const { user, editLink } = useContext();

  const link = editLink ? editLink : defaultEditLink;

  const [categoriesResult, reexecuteCategoriesQuery] = useQuery<Result>({
    query: GetCategoriesQuery,
    variables: { username: user.username },
    pause: !user.loggedIn,
  });

  const [createLinkResult, createLink] = useMutation(CreateLink);
  const [updateLinkResult, updateLink] = useMutation(UpdateLink);
  const [deleteLinkResult, deleteLink] = useMutation(DeleteLink);

  const categories = React.useMemo(() =>  {
    if (categoriesResult.data) {
      return categoriesResult.data.categories.map((category: Category) => ({id: category.id, title: category.title}))
    }

    return [];
  }, [categoriesResult]);

  const formik = useFormik<Values>({
    enableReinitialize: true,
    initialValues: {
      title: link.title,
      description: link.description,
      href: link.href,
      categories: link.categories,
    },
    validationSchema: Yup.object({
      title: Yup.string()
        .min(1, 'Must be 1 character or more')
        .max(64, 'Must be less than 64 characters')
        .required('Required'),
      description: Yup.string()
        .max(256, 'Must be less than 256 characters'),
      href: Yup.string()
        .url('No Valid url')
        .required('Required'),
      categories: Yup.array()
        .min(1, 'Link must be added to at least one category')
        .required(),
    }),
    onSubmit: (values: Values) => {
      if (link.id) {
        const variables = { id: link.id, ...values };
        updateLink(variables).then((result) => {
          if (result.error) {
            getGqlErrors(formik, result.error);
          } else {
            onSubmit();
            formik.resetForm();
          }
        });
      } else {
        createLink(values).then((result) => {
          if (result.error) {
            getGqlErrors(formik, result.error);
          } else {
            onSubmit();
            formik.resetForm();
          }
        });
      }
    },
  });

  const handleDelete = () => {
    deleteLink({ id: link.id }, { additionalTypenames: ['Link'] })
      .then((result) => {
        if (result.error) {
          getGqlErrors(formik, result.error);
        } else {
          onSubmit();
        }
    });
  };

  const getSelected = () => {
    const selected = categories.find((c: Category) => c.id === formik.values.categories[0]);
    return selected === undefined ? '' : selected.id;
  };

  return (
    <>
      <h2 className="text-lg font-semibold text-primary-txt">
        Add/Edit link
      </h2>
      <form onSubmit={formik.handleSubmit} className="mt-2">
        <div>
          <label htmlFor="title" className="sr-only">Title</label>
          <input
            className={`border-2 rounded w-full py-3 px-3 text-primary-txt mb-1 mt-4 outline-none ${hasError(formik, 'title') ? 'border-warning' : ''}`}
            id="title"
            type="text"
            required
            placeholder="Title"
            {...formik.getFieldProps('title')}
          />
          <span hidden={!hasError(formik, 'title')} className="text-sm text-warning">{getHelperText(formik, 'title')}</span>

          <label htmlFor="href" className="sr-only">Link</label>
          <input
            className={`border-2 rounded w-full py-3 px-3 text-primary-txt mb-1 mt-4 outline-none ${hasError(formik, 'href') ? 'border-warning' : ''}`}
            id="href"
            type="url"
            required
            placeholder="Link"
            {...formik.getFieldProps('href')}
          />
          <span hidden={!hasError(formik, 'href')} className="text-sm text-warning">{getHelperText(formik, 'href')}</span>

          <label htmlFor="categories" className="sr-only">Category</label>
          <div className="relative inline-flex mb-1 mt-4 w-full">
            <svg className="w-4 h-4 absolute top-0 right-0 m-4 pointer-events-none" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 412 232">
              <path d="M206 171.144L42.678 7.822c-9.763-9.763-25.592-9.763-35.355 0-9.763 9.764-9.763 25.592 0 35.355l181 181c4.88 4.882 11.279 7.323 17.677 7.323s12.796-2.441 17.678-7.322l181-181c9.763-9.764 9.763-25.592 0-35.355-9.763-9.763-25.592-9.763-35.355 0L206 171.144z" fill="currentColor" fillRule="nonzero" />
            </svg>
            <select
              id="categories"
              className={`border-2 rounded py-3 pl-2 pr-10 bg-white focus:outline-none appearance-none w-full ${hasError(formik, 'categories') ? 'border-warning' : ''}`}
              onChange={(e) => formik.setFieldValue('categories', [e.target.value])}
              value={getSelected()}
            >
              <option value="" selected disabled hidden>Choose category</option>
              {categories.map((c: Category) => (
                <option key={c.id} value={c.id}>{c.title}</option>
              ))}
            </select>
          </div>
          <span hidden={!hasError(formik, 'categories')} className="text-sm text-warning">{getHelperText(formik, 'categories')}</span>

          <label htmlFor="description" className="sr-only">Description</label>
          <textarea
            className={`border-2 rounded w-full py-3 px-3 text-primary-txt mb-1 mt-4 outline-none ${hasError(formik, 'description') ? 'border-warning' : ''}`}
            id="description"
            placeholder="Add a nice description here..."
            rows={2}
            {...formik.getFieldProps('description')}
          />
          <span hidden={!hasError(formik, 'description')} className="text-sm text-warning">{getHelperText(formik, 'description')}</span>
        </div>

        <div className="flex justify-end mt-4">
          { link.id && (
            <>
              <label htmlFor="delete" className="sr-only">Delete</label>
              <button
                id="delete"
                type="button"
                className="w-8 h-8 focus:outline-none text-primary-txt"
                onClick={handleDelete}
              >
                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                  <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16" />
                </svg>
              </button>
            </>
          )}
          <div className="flex-auto" />
          <button
            type="button"
            className="flex-none text-gray-800 px-4 py-2 rounded-full border-2 rounded-full font-semibold focus:outline-none"
            onClick={onCancel}
          >
            Cancel
          </button>
          <button
            type="submit"
            className="flex-none bg-primary hover:bg-primary-darker text-white px-6 py-2 rounded-full font-semibold ml-4"
          >
            Save
          </button>
        </div>
      </form>
    </>
  );
};

export default LinkCard;
