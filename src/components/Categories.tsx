import React from 'react';
import union from 'lodash/union';
import filter from 'lodash/filter';

import { useQuery } from 'urql';
import { useContext } from '../context';
import { Category, Link } from './Components';
import { useHistory } from 'react-router-dom';

export const GetCategoriesQuery = `
  query GetCategories($username: String!) {
    categories(username: $username) {
      id  
      title
      titleSlug
      description
      visibility
      links {
        id
        title
        description
        href
        userRef
      }
    }
  }
`;

interface Result {
  categories: Array<Category>
}

const Categories = () => {
  const { user, saveEditLink } = useContext();
  const history = useHistory();

  const [displayCategory, setDisplayCategory] = React.useState<Array<string>>([]);

  const [categoriesResult, reexecuteCategoriesQuery] = useQuery<Result>({
    query: GetCategoriesQuery,
    variables: { username: user.username },
    pause: !user.loggedIn,
  });

  const isHidden = (id: string) => {
    return displayCategory.indexOf(id) < 0;
  }

  const handleEdit = (category: Category, link: Link) => {
    saveEditLink({
      id: link.id,
      title: link.title,
      href: link.href,
      description: link.description,
      categories: [category.id],
    })
    history.push('/link');
  };

  return (
    <>
      {categoriesResult.data && categoriesResult.data.categories.map((category: Category) => (
        <div key={category.id}>
          <div
            className="flex cursor-pointer mt-2"
            onClick={() => {
              if (isHidden(category.id)) {
                setDisplayCategory(union(displayCategory, [category.id]))
              } else {
                setDisplayCategory(filter(displayCategory, (id) => id !== category.id ))
              }
            }}
          >
            {isHidden(category.id) ? (
              <div className="flex-none">
                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" className="w-4 h-4 mt-1">
                  <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M9 5l7 7-7 7" />
                </svg>
              </div>
            ) : (
              <div className="flex-none">
                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" className="w-4 h-4 mt-1">
                  <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M19 9l-7 7-7-7" />
                </svg>
              </div>
            )}
            <div className="flex-1 pl-2 align-middle">
              <h2 className="font-semibold">
                {category.title}
              </h2>
            </div>
          </div>

          <ul className="list-disc pl-8" hidden={isHidden(category.id)}>
            {category.links.map((link: Link) => (
              <li key={link.id}>
                <a href={link.href} target="_blank" rel="noopener noreferrer" className="text-link-txt">
                  {link.title}
                </a>
                {` - ${link.description}`}
                <button
                  type="button"
                  className="text-sm font-semibold ml-2"
                  onClick={() => handleEdit(category, link)}
                  >
                  | Click to edit
                </button>
              </li>
            ))}
          </ul>
        </div>
      ))}
    </>
  )
};

export default Categories;
