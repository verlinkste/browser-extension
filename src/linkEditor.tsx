import React from 'react';

import LinkCard from './components/LinkCard';

import { useHistory } from 'react-router-dom';

const LinkEditor = () => {
  const history = useHistory();

  const goBackHome = () => {
    history.goBack();
  }

  return (
    <div className="container py-2 px-4">
      <LinkCard
        onCancel={goBackHome}
        onSubmit={goBackHome}
      />
    </div>
  )
};

export default LinkEditor;
