import React from 'react';
import { browser, Tabs } from 'webextension-polyfill-ts';

import Categories from './components/Categories';
import { useHistory } from 'react-router-dom';
import { EditLink } from './context/link';
import { useContext } from './context';

const Home = () => {
  const { saveEditLink } = useContext();

  const history = useHistory();

  const [activeTab, setActiveTab] = React.useState<Tabs.Tab | null>(null);

  function onGot(tabInfo: Array<Tabs.Tab>) {
    const tab = tabInfo[0];

    setActiveTab(tab);

    const link: EditLink = {
      id: '',
      title: tab?.title ? tab.title : '',
      href: tab?.url ? tab.url : '',
      description: '',
      categories: [],
    }
    saveEditLink(link);
  }

  function onError(error: any) {
    // TODO add error logging
    console.log(`Error: ${error}`);
  }

  React.useEffect(() => {
    const gettingCurrent = browser.tabs.query({active: true});
    gettingCurrent.then(onGot, onError);
  }, []);

  return (
    <div className="container py-2 px-4">
      <button
        className="bg-primary text-primaryContrastText border border-primary-darker shadow rounded py-2 w-full truncate"
        onClick={() => history.push('/link')}
      >
        Add <span className="font-semibold">{activeTab && activeTab.title}</span>
      </button>
      <Categories />
    </div>
  )
};

export default Home;
