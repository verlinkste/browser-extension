import React from 'react';
import { Router, Route, Switch } from 'react-router-dom';
import { createMemoryHistory } from 'history';
import Home from './Home';
import SignIn from './Signin';
import { useContext } from './context';
import LinkEditor from './linkEditor';
import axios from './helpers/axios';
import { defaultUser, User } from './context/user';

const history = createMemoryHistory();

const Navigation = () => {
  const { user, saveUser } = useContext();

  React.useEffect(() => {
    if (history.location.pathname === '/' && user.loggedIn) {
      history.push('/home');
    }
  }, [user]);

  React.useEffect(() => {
    axios.get('/auth/is-authenticated')
      .catch((error) => {
        if (error.response.status === 401) {
          // session cookie is no longer valid logout user
          const user: User = {
            ...defaultUser,
            loggedIn: false,
          };
          saveUser(user);
          history.push('/');
        }
      })
  }, []);

  return (
    <Router history={history}>
      <Switch>
        <Route path="/home">
          <Home />
        </Route>
        <Route path="/link">
          <LinkEditor />
        </Route>
        <Route path="/">
          <SignIn />
        </Route>
      </Switch>
    </Router>
  );
}

export default Navigation;
