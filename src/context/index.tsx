import React from 'react';
import produce from 'immer';
import Storage from 'versioned-storage';

import {
  defaultUser, User, resetUser as resetUserImpl, saveUser as saveUserImpl
} from './user';
import { defaultEditLink, EditLink, resetEditLink as resetEditLinkImpl, saveEditLink as saveEditLinkImpl } from './link';

export interface State {
  user: User
  editLink: EditLink
  resetUser(): void
  saveUser(user: User): void
  resetEditLink(): void
  saveEditLink(link: EditLink): void
}

interface DraftState {
  [key: string]: any;
}

export const defaultState = {
  user: defaultUser,
  editLink: defaultEditLink,
};

export const Context = React.createContext({ ...defaultState } as State);

export const useContext = () => React.useContext(Context);

const stateVersion = 1;

const loadState = () => {
  let state = null;

  try {
    const stateStorage = new Storage<State>('state', stateVersion);
    state = stateStorage.read();
  } catch (error) {
    // TODO Add error logging
    console.log(error);
  }

  return state === null ? defaultState : state;
};

const saveState = (state: any) => {
  try {
    const stateStorage = new Storage<State>('state', stateVersion);
    stateStorage.write(state);
  } catch (error) {
    // TODO Add error logging
    console.log(error);
  }
};

export class StateProvider extends React.Component {
  state = loadState();

  componentDidUpdate(prevProps: any, prevState: { state: { user: User; }; }) {
    if (this.state !== prevState.state) {
      saveState(this.state);
    }
  }

  handleUpdateState = (key: string, value: any): void => {
    const nextState = produce(this.state, (draftState: DraftState) => {
      draftState[key] = value;
    });
    this.setState(nextState);
  }

  resetUser = (): void => {
    resetUserImpl(this.handleUpdateState);
  }

  saveUser = (user: User): void => {
    saveUserImpl(this.handleUpdateState, user);
  }

  saveEditLink = (link: EditLink): void => {
    saveEditLinkImpl(this.handleUpdateState, link);
  }

  resetEditLink = (): void => {
    resetEditLinkImpl(this.handleUpdateState)
  }

  render(): JSX.Element {
    const { editLink, user } = this.state;
    const { resetEditLink, resetUser, saveEditLink, saveUser } = this;

    return (
      <Context.Provider value={{
        editLink, resetEditLink, saveEditLink,
        user, resetUser, saveUser,
      }}
      >
        { this.props.children }
      </Context.Provider>
    );
  }
}
