export interface EditLink {
  id: string
  title: string
  href: string
  description: string
  categories: Array<string>
}

export const editLinkKey = 'editLink';

export const defaultEditLink: EditLink = {
  id: '',
  title: '',
  href: '',
  description: '',
  categories: [],
};

export const resetEditLink = (updateState: (key: string, value: any) => void) => {
  updateState(editLinkKey, defaultEditLink);
};

export const saveEditLink = (updateState: (key: string, value: any) => void, link: EditLink) => {
  updateState(editLinkKey, link);
};
