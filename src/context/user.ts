export interface User {
  id: string
  username: string
  loggedIn: boolean
}

export const userStateKey = 'user';

export const defaultUser = {
  id: '',
  username: '',
  loggedIn: false,
} as User;

export const resetUser = (updateState: (key: string, value: any) => void) => {
  updateState(userStateKey, defaultUser);
};

export const saveUser = (updateState: (key: string, value: any) => void, user: User) => {
  updateState(userStateKey, user);
};
