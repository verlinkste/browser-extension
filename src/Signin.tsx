import React from 'react';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { useHistory } from 'react-router-dom';

import axios from './helpers/axios';

import { hasError, getResponseErrors, getHelperText } from './helpers/formik';
import { useContext } from './context';
import { User } from './context/user';

const SignIn = () => {
  const history = useHistory();

  const { saveUser } = useContext();

  const formik = useFormik({
    initialValues: {
      email: '',
      password: '',
    },
    validationSchema: Yup.object({
      email: Yup.string()
        .email('Invalid email address')
        .required('Required'),
      password: Yup.string()
        .min(8, 'Must be 8 characters or more')
        .required('Required'),
    }),
    onSubmit: (values) => {
      axios.post('/auth/login', values)
        .then((response) => {
          const user: User = {
            loggedIn: true,
            ...response.data,
          };
          saveUser(user);
          history.push('/home');
        })
        .catch((error) => {
          formik.setErrors(getResponseErrors(error));
        });
    },
  });

  return (
    <div className="flex items-center justify-center py-4 px-4 sm:px-6 lg:px-8">
      <div className="max-w-md w-full space-y-8">
        <div>
          <img
            className="mx-auto h-12 w-12 rounded-full"
            src="/logo.svg"
            alt="Verlinkste"
          />
          <h2 className="mt-6 text-center text-3xl font-extrabold text-gray-900">
            Sign in to your account
          </h2>
        </div>
        <form className="mt-8 space-y-6" onSubmit={formik.handleSubmit}>
          <input type="hidden" name="remember" value="true" />
          <div className="rounded-md shadow-sm -space-y-px">
            <div>
              <label htmlFor="email" className="sr-only">Email Address</label>
              <input
                id="email"
                type="email"
                autoComplete="email"
                required
                className={`${hasError(formik, 'email') ? 'border-warning' : 'border-gray-300'} appearance-none rounded w-full px-3 py-3 border-2 placeholder-gray-500 text-primary-txt rounded-t-md focus:outline-none focus:ring-primary focus:border-primary sm:text-sm mb-1`}
                placeholder="Email Address"
                {...formik.getFieldProps('email')}
              />
              <span hidden={!hasError(formik, 'email')} className="text-sm text-warning">{getHelperText(formik, 'email')}</span>
            </div>
            <div>
              <label htmlFor="password" className="sr-only">Password</label>
              <input
                id="password"
                type="password"
                autoComplete="current-password"
                required
                className={`${hasError(formik, 'password') ? 'border-warning' : 'border-gray-300'} mt-4 appearance-none rounded w-full px-3 py-3 border-2 placeholder-gray-500 text-primary-txt rounded-t-md focus:outline-none focus:ring-primary focus:border-primary focus:z-10 sm:text-sm mb-1`}
                placeholder="Password"
                {...formik.getFieldProps('password')}
              />
              <span hidden={!hasError(formik, 'password')} className="text-sm text-warning">{getHelperText(formik, 'password')}</span>
            </div>
          </div>

          <div>
            <button
              type="submit"
              className="group relative w-full flex justify-center py-2 px-4 border border-transparent text-sm font-medium rounded-md text-white bg-primary hover:bg-primary-darker focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-primary"
            >
            <span className="absolute left-0 inset-y-0 flex items-center pl-3">
              <svg
                className="h-5 w-5 text-white"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 20 20"
                fill="currentColor"
                aria-hidden="true"
              >
                <path
                  fillRule="evenodd"
                  d="M5 9V7a5 5 0 0110 0v2a2 2 0 012 2v5a2 2 0 01-2 2H5a2 2 0 01-2-2v-5a2 2 0 012-2zm8-2v2H7V7a3 3 0 016 0z"
                  clipRule="evenodd"
                />
              </svg>
            </span>
              Sign in
            </button>
          </div>
        </form>
      </div>
    </div>
  );
};

export default SignIn;
