import axiosPackage from 'axios';

const axios = axiosPackage.create({
  baseURL: 'https://verlinkste.com/backend',
  headers: {
    'Content-Type': 'application/json',
  },
  withCredentials: true,
});

export default axios;
