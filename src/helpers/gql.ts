import { createClient } from 'urql';


const client = createClient({
  url: `https://verlinkste.com/backend/graphql`,
  fetchOptions: () => ({
    credentials: 'include',
  }),
});

export default  client;
