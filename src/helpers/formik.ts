interface Errors {
  [key: string]: string;
}

interface Response {
  data: Errors
}

interface ErrorResponse {
  response: Response
}

export const hasError = (formik: any, field: string) => formik.touched[field] && !!formik.errors[field];

export const getHelperText = (formik: any, field: string) => {
  if (formik.touched[field] && !!formik.errors[field]) {
    return formik.errors[field];
  }
  return null;
};

export const getResponseErrors = (error: ErrorResponse) => {
  const errors: Errors = {};

  if (error.response && error.response.data) {
    Object.keys(error.response.data).forEach((key) => {
      errors[key] = error.response.data[key];
    });
  }

  return errors;
};

export const getGqlErrors = (formik: any, errors: any) => {
  let allErrors = {};
  try {
    errors.graphQLErrors.forEach((error: any) => {
      try {
        const err = JSON.parse(error.originalError.message);
        allErrors = { ...allErrors, ...err };
      } catch (e) {
        // TODO add error logging
        console.log(e);
      }
    });
  } catch (e) {
    // TODO add error logging
    console.log(e);
  }

  formik.setErrors(allErrors)
};
